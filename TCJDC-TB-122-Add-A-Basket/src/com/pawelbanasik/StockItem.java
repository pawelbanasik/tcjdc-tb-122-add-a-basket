package com.pawelbanasik;

// implementuje interface do porownywania - bedzie powownywal StockItem
public class StockItem implements Comparable<StockItem> {
	private final String name;
	private double price;
	private int quantityStock;

	public StockItem(String name, double price) {
		this.name = name;
		this.price = price;
		// konstruktor inicjalizuje na zero quantityStock
		this.quantityStock = 0;
	}
	
	
	// przeciaza konstruktor tworzac kolejny
	public StockItem(String name, double price, int quantityStock) {
		this.name = name;
		this.price = price;
		// w tym drygim juz quantityStock jest pobierane z pola;
		this.quantityStock = quantityStock;
	}



	public double getPrice() {
		return price;
	}	

	public void setPrice(double price) {
		// zeby nie mozna bylo dodawac zero/walidacja
		if (price > 0.0) {
			this.price = price;
		}
	}

	public String getName() {
		return name;
	}

	public int quantityInStock() {
		return quantityStock;
	}

	public void adjustStock(int quantity) {
		// pole plus parametr
		int newQuantity = this.quantityStock + quantity;
		// jezeli parametr wiekszy od zera to pole quantityStock bedzie zwiekszone
		if (newQuantity >= 0) {
			this.quantityStock = newQuantity;
		}
	}
	// bedzie porownywal dlatego nadpisuje metode equals
	@Override
	public boolean equals(Object obj) {
		System.out.println("Entering StockItem.equals");

		if (obj == this) {
			return true;
		}

		if ((obj == null) || (obj.getClass() != this.getClass())) {
			return false;
		}

		String objName = ((StockItem) obj).getName();
		return this.name.equals(objName);
	}

	@Override
	public int hashCode() {
		// ta liczba moze byc jakakolwiek to po prostu dodaje do
		// naszego hashCode liczbe
		return this.name.hashCode() + 31;
	}

	@Override
	public int compareTo(StockItem o) {
		System.out.println("entering StockItem.compareTo");
		if (this == o) {
			return 0;
		}
		if (o != null) {
			return this.name.compareTo(o.getName());
		}

		throw new NullPointerException();

	}

	@Override
	public String toString() {
		return this.name + " : price " + this.price;
	}

	
}
