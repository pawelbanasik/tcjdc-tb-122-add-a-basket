package com.pawelbanasik;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

// klasa koszyk jest po to zeby nie robic w StockItem
// dla kazdego klienta pola
public class Basket {
	private final String name;
	private final Map<StockItem, Integer> map;

	public Basket(String name) {
		this.name = name;
		this.map = new HashMap<>();
	}

	public int addToBasket(StockItem item, int quantity) {
		if ((item != null) && (quantity > 0)) {
			// Java 8 - pobieram wartosc pod kluczem
			// klucz nie moze byc nullem
			int inBasket = map.getOrDefault(item, 0);
			map.put(item, inBasket + quantity);
			return inBasket;
		}
		return 0;
	}

	// mapa read-only jest po zastosowaniu tego
	public Map<StockItem, Integer> items() {
		return Collections.unmodifiableMap(map);
	}

	@Override
	public String toString() {
		String s = "\nShopping basket " + name + " contains " + map.size() + " items\n";
		double totalCost = 0.0;
		for (Map.Entry<StockItem, Integer> item : map.entrySet()) {
			s = s + item.getKey() + ". " + item.getValue() + " purchased\n";
			totalCost += item.getKey().getPrice() * item.getValue();
		}
		return s + "Total cost " + totalCost;
	}

}
